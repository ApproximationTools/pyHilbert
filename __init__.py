"""

pyHilbert: A Pythonic abstraction of vector spaces with dot-products and norms 
==============================================================================

Contents
--------
pyHilbert contains four essential entities


 Space
 Vector
 LinOp
 Basis

"""

from .pyhilbert import *
