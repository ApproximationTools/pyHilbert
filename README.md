# pyHilbert
### A Pythonic abstraction of vector spaces with dot-products and norms (differing from the usual Cartesian / $`\ell^2`$ dot-products) 

Mathematics is messy. But the code doesn't have to be.

**Disclaimer: _This ReadMe is more of an aspirational manifesto than an actual guide. Seeing as I'm the only one really using this code still (until it's ready to really be launched, I'm using the Readme as a kind of ideas scratch space)_**

Functions and linear operators in functional analysis boil down to vectors and matrices in any numerical approximation. This is a library that essentially mimics the behaviour of NumPy, but allows for different function spaces by extending the notion of what a dot product is. Thus the library helps build, manipulate, and solve equations of such functions and linear operators using Pythonic operations, particularly the (relatively) new matrix multiplication operator ```@```. There is planned support for a variety of dot-products, enabling analysis in a variety of function spaces common in numerical analysis, for example the Sobolev spaces $`H^k`$...

There are a few concepts to tackle. Firstly all spaces are inherently finite, evidently, they are encapsulated in the

### ```Space``` and ```Vector``` classes

In this packages a "space" is defined as the span of some fixed set of fundamental elements, for example finite-elements on some triangulationor a Fourier basis. The ```Space``` class knows the form of $`\varphi_1,\ldots,\varphi_N`$ and further-more the exact inner-product between these elements, which is stored in a Gram or stiffness matrix $`\mathbf{G}`$ where $`\mathbf{G}_{i,j}=\langle \varphi_i, \varphi_j\rangle`$. For example our ```Space``` may be (a linear subset of) the Sobolev space $`H_0^1(D)`$ where the $`\varphi_i`$ are the hat functions on some triangulation of the domain $`D`$. 

A ```Vector``` in this ```Space``` can be described by an array of $`N`$ numbers, and for some vector $`v`$ we have 
```math
v = \sum_{i=1}^N \mathbf{v}_i \varphi_i
```
Given two ```Vectors``` $`u, v`$ which have array representations $`\mathbf{u}, \mathbf{v}`$, the inner-product is
```math
\langle u, v \rangle 
= \left \langle \sum_{i=1}^N \mathbf{u}_i \varphi_i, \sum_{j=1}^N \mathbf{v}_j \varphi_j \right \rangle
= \sum_{i,j=1}^N \mathbf{u}_i \mathbf{v}_j \langle \varphi_i, \varphi_j \rangle
= \mathbf{u}^T \mathbf{G} \mathbf{v} \, ,
```
thus the Gram matrix of the ```Space``` class can be used to quickly compute inner-products. Given two objects ```u``` and ```v``` of type ```Vector```, the inner-product can be computed by ```u.dot(v)``` or ```v.dot(u)```.

The ```Vector``` class thus internally is contains the array of values of size $`N`$, and a reference to the ```Space``` class that it is a member of. Inner-products between vectors is only allowed if they both refer to the same ```Space```. The ```Vector``` class also supports the full suite of arithmetic operations ```+```, ```-```, ```+=``` and such, noting that ```*``` and ```/``` only apply between scalars and vectors, not vectors and vectors.

### ```LinOp``` class

This is the class of linear operators, where the ```@``` symbol is implemented, so that a ```LinOp``` can act on a ```Vector```, or equivalently several ```LinOp```'s can be composed provided their right and left type are compatible, i.e. $`A ( B v )`$ is computed with ```A @ B @ v```. Note that there are no optimisations for performance with how operations are associated: it may be beneficial for example to include your own parentheses to ensure operation order, e.g. ```(A @ B) @ v``` could be faster than ```A @ (B @ v)``` for whatever reason (just the same as grouping matrix multiplication can be beneficial for performance in certain circumstances).

There is also the notion of _adjoint_ (similar to a conjugate transpose for a matrix). In the mathematical literature a transpose is written $`A^*`$ or sometimes $`A^H`$. Here, to avoid notation clashes, a ```LinOp```, ```F```, has adjoint ```F.A```

### ```Basis``` class

The ```Basis``` class inherits from ```LinOp```, mainly as typically an $`n`$ dimensional basis can be seen as an operator from $`\mathbb{R}^n`$ to a vector space $`V`$. Here we have the notion of column-vectors from matrice implemented.

## TODO list

 - Look in to NumPy ```view``` objects for transposes and axis permutations. Could be useful for implementation here
 - Perhaps chage ```.A``` for ```.T``` to become fully interoperable with NumPy, but then also implement vectors column-wise properly with the internal data structures...
 - Potentially move dot product in to ```space``` class,
 - Implement the ```space``` class as some form of global static (if possible?) class such that each ```Vector``` in that space refers to the same instance.
 - Fix __init__ and importing structure, and implement setup.py to make this a viable package, implement this https://python-packaging.readthedocs.io/en/latest/dependencies.html

### Some rambling notes from a previous iteration of this code

So what approach would we want to use if we wanted to solve a simple diffusion equation like 
```math
-\nabla\cdot(a(x)\nabla u)=f(x))
```

where $`a(x)`$ and $`f(x)`$ are given and we want to solve for $u$

```
    function_space = pat.FEM(nodes = node_locs, norm=pat.H1_0)
```

Then we can construct the operator. The operator ```D```, defined in the library, represents $`\nabla`$
```
    D = pat.linear_operators.nabla(function_space)
```
The functions must be defined in some way on this triangulation
```
    f = pat.Function(np.ones(function_space.shape), function_space)
    a = pat.Function(np.random.random(function_space.shape), function_space)
```

Then we can define ``` A = - D @ a @ D ```, and solving the PDE is now the system ``` A @ u = f ```, which we have direct access to by calling 

``` 
    A = - D @ a @ D
    u_raw = np.linalg.solve(A.matrix_rep(), f.vector_rep()) 
    u = pat.Function(u_raw, function_space)
```

The advantages of this approach are that it gives the user control of many aspects of the problem. Different PDEs and operators all have their unique issues and instabilities. There is a variety of decompositions or techniques that trained numerical analysts might want to use, but don't want to have to re-invent the wheel of all this support machinery.

Many other PDE libraries, although highly powerful and capable of things like adaptive mesh routines, whizz-bang solvers, forget-about-the-details interpolators, hide various intermediary steps that could be of use to mathematicians.

It is hoped that this library will bridge the gap between numerical analysts who want to get their hands dirty and analyse their algorithms and methods, and practitioners who just want effective PDE solutions with clean Pythonic syntax and stright-forward error reporting.

Have you got your own tricky hyperbolic operator for which you want a custom discretisation scheme that balances gawd-knows-what terms? No problem - just bake the scheme in the the ```pat.Operator``` class using templates. You are guaranteed an operator which you can then combine with others and get the expected linear schemes, no funny business or hidden optimisation.

