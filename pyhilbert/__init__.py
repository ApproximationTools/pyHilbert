"""

pyHilbert: A Pythonic abstraction of vector spaces with dot-products and norms 
==============================================================================

Contents
--------
pyHilbert contains four essential entities


 Space
 Vector
 LinOp
 Basis

"""

from .space import *
from .linop import *
from .vector import *
from .basis import *
from .solver import *
