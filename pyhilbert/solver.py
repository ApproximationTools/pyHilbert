"""
solvers.py

Author: James Ashton Nichols
Start date: July 2018 

Some utils including a first implementation of the FEM scheme
"""

import math
import numpy as np
import scipy.linalg
import scipy.sparse
import scipy.optimize
import scipy.sparse.linalg
import warnings

from pyhilbert.pyhilbert.space import *
from pyhilbert.pyhilbert.vector import *
from pyhilbert.pyhilbert.basis import *

__all__ = ['DyadicFEMSolver', 'DyadicGuesser', 'mod_H1_dot_matrix', 'int_L2_H1_basis']

class DyadicFEMSolver(object):
    """ Solves the -div( a nabla u ) = f PDE on a grid, with a given by 
        some random / deterministic field, with dirichelet boundary conditions """

    def __init__(self, fem_space, rand_field, f):
       
        self.fem_space = fem_space
        self.field_space = rand_field.space
        self.L2_fem_space = L2DyadicSq(div=self.fem_space.div)

        self.div = fem_space.div
        # -1 as we are interested in the grid points, of which there is an odd number
        self.n_side = 2**self.div - 1
        self.n_el = self.n_side * self.n_side
        self.h = 2**(-self.div)

        self.A = mod_H1_dot_matrix(rand_field, self.fem_space, self.L2_fem_space)
        self.f = int_L2_H1_basis(f, self.L2_fem_space)
        
        self.u = Vector.zero(space=self.fem_space)

    def solve(self):
        """ The bilinear form simply becomes \int_D a nab u . nab v = \int_D f """
        u_flat = scipy.sparse.linalg.spsolve(self.A, self.f)
        # Pad the zeros on each side... (due to the boundary conditions) and make the 
        # dyadic piecewise linear function object
        self.u.values = u_flat

def mod_H1_dot_matrix(mod, fem_space, L2_fem_space):
    # Makes an appropriate sized field for our FEM grid
    a = L2_fem_space.interp(mod).values.reshape((L2_fem_space.side_len, L2_fem_space.side_len))
    
    # Now we make the various diagonals
    diag = (a[:-1, :-1] + a[:-1,1:] + a[1:,:-1] + a[1:, 1:]).flatten()
   
    # min_diag is below the diagonal, hence deals with element to the left in the FEM grid
    lr_diag = -0.5 * (a[1:, 1:] + a[:-1, 1:]).flatten()
    lr_diag[fem_space.side_len-1::fem_space.side_len] = 0 # These corresponds to edges on left or right extreme
    lr_diag = lr_diag[:-1]
    
    # Far min deals with the element that is above
    ud_diag = -0.5 * (a[1:-1, 1:] + a[1:-1, :-1]).flatten()

    return scipy.sparse.diags([diag, lr_diag, lr_diag, ud_diag, ud_diag], 
                              [0, -1, 1, -fem_space.side_len, fem_space.side_len]).tocsr()

# MAKE THESE THINGS IN TO A MAD MATRIX OPERATION Y'ALL
# MAYBE IN THE SPACE CLASS
def int_L2_H1_basis(f, L2_fem_space):
    
    if np.isscalar(f):
        f_sq = f * np.ones((L2_fem_space.side_len, L2_fem_space.side_len)) 
    elif isinstance(f, Vector):
        f_interp = L2_fem_space.interp(f)
        f_sq = f_interp.values.reshape((L2_fem_space.side_len, L2_fem_space.side_len))
    else:
        f_sq = f.reshape((L2_fem_space.side_len, L2_fem_space.side_len))

    # We assume that, at the end of this, we get a square array at the FEM level
    f_lc = f_sq[:-1,:-1]/6. + f_sq[:-1,1:]/3. + f_sq[1:,:-1]/3. + f_sq[1:,1:]/6.  

    return f_lc.flatten() / (L2_fem_space.side_len * L2_fem_space.side_len)

class DyadicGuesser(object):
    """ Effectively solves the linear system to take a good guess at the parameters
        that are behind some given approximate solution of a PDE """

    def __init__(self, field_mean, field_mult, fem_space, field_space, field_basis=None, y_bounds=None, pde_rhs=None):

        self.fem_space = fem_space
        self.field_space = field_space
        self.L2_fem_space = L2DyadicSq(div=fem_space.div)

        self.field_mean = field_mean
        self.field_mult = field_mult
        
        # generate the psis from the L2 basis thing
        if field_basis is not None:
            self.psis = field_basis
        else:
            self.psis = Basis.eye(self.field_space)

        self.H_psis = [mod_H1_dot_matrix(psi, self.fem_space, self.L2_fem_space) for psi in self.psis]

        self.y_bounds = y_bounds
    
        if pde_rhs is not None:
            self.pde_rhs = int_L2_H1_basis(pde_rhs, self.L2_fem_space)
        else:
            self.pde_rhs = int_L2_H1_basis(1.0, self.L2_fem_space)

        self.nab_inv_rhs = - Vector.make_riesz_rep(self.pde_rhs, space=self.fem_space)
                        
    def nearest_params_resid(self, u_star, y_bounds=None):

        phi_0 = self.nab_inv_rhs + self.field_mean * u_star

        phi_j_vecs = []
        for H_psi in self.H_psis:
            phi_j = -Vector.make_riesz_rep(H_psi @ u_star.values, space=self.fem_space)
            phi_j_vecs.append(phi_j)

        phi_js = Basis(phi_j_vecs, space=self.fem_space)

        phi_0_proj, y_guess = phi_js.project(phi_0 / self.field_mult, return_coeffs = True)

        y_bounds = self.y_bounds if y_bounds is None else y_bounds
        if y_bounds is not None:
            
            # If we have set bounds on the parameters, then we check that our 
            # guess y_guess is within these bounds.
            
            y_0 = y_guess.clip(y_bounds[:,0], y_bounds[:,1])
            
            if any(y_guess != y_0):
                # y_guess is not within the bounds, so we need to use a 
                # constrained optimizer
                res = scipy.optimize.lsq_linear(phi_js.G, (phi_js.A @ phi_0) / self.field_mult, y_bounds.T) 
                if not res.success:
                    warnings.warn(f'{self.__class__.__name__}: LSQ not converged, square sum = {res.cost:0.3e}')
                y_guess = res.x

        phi_0_proj = phi_js @ (self.field_mult * y_guess)
        residual = (phi_0 - phi_0_proj).norm()

        return y_guess, residual

    def nearest_resid(self, y, w, w_coeffs, W):

        # Here we solve the other problem, what is the nearest point to a valid
        # solution, in the residual space

        # First we must create the modified W vectors
        # Two options: from an L2 or an H1 basis
        a = Vector.ones(self.field_space) * self.field_mean + self.psis @ (self.field_mult * y)
        A_y = mod_H1_dot_matrix(a, self.fem_space, self.L2_fem_space)

        W_hat_vals = np.zeros((self.fem_space.n, W.n))
        # L2 case
        for i, om in enumerate(W):
            om_L2 = int_L2_H1_basis(om.values, self.L2_fem_space)
            W_hat_vals[:, i] = scipy.sparse.linalg.spsolve(A_y, om_L2)
        # H1 case
        #for i, om in enumerate(W):
        #    W_hat_vals[:, i] = Vector(scipy.sparse.linalg.spsolve(A_y, self.fem_space.G @ om), self.fem_space)
        
        W_hat = Basis(W_hat_vals, space = self.fem_space)
        w_hat = W_hat.dot_to_projection(w_coeffs)

        rhs_inv_hat = W_hat.project(self.nab_inv_rhs)

        u_star_hat = w_hat + rhs_inv_hat - self.nab_inv_rhs

        # Now we have to transform it back to a u...
        u_star = Vector(scipy.sparse.linalg.spsolve(A_y, self.fem_space.G @ u_star_hat.values), self.fem_space)    
        
        residual = (w_hat + rhs_inv_hat).norm()

        return u_star, residual
