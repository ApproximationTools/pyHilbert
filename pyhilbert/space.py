"""
space.py

Author: James Ashton Nichols
Start date: June 2018

The abstraction of a space
"""

import math
import numpy as np
import scipy.interpolate
import scipy.linalg
import scipy.sparse
import copy

import warnings

import matplotlib.pyplot as plt
from matplotlib import cm 
from mpl_toolkits.mplot3d import axes3d, Axes3D

#from pyhilbert.pyhilbert.vector import *
#from pyhilbert.pyhilbert.basis import *

__all__ = ['Space', 'l2', 'L2DyadicSq', 'H1DyadicSq']

def multisel(sel, n, i):
    return (slice(None),)*i + (sel,) + (slice(None),)*(n-i-1)

class Space(object):
    """
    This is the abstraction of a Space, which underlies any vector. It's principle 
    use is the definition of the Gram matrix between any element in the space. 
    Then dot products of vectors in this space are easily computed using this.
    """
    def __init__(self, n=None):
        self._n = n
        self._G = None
        
    @property
    def G(self):
        return self._G

    @property
    def n(self):
        return self._n

    def check_values_shape(self, values, axis):
        """ ensures values are of right size for space if relevant """
        if self.n:
            return values.shape[axis] == self.n 
        else:
            return True

    def values_format(self, values, axis=None):
        """ ensures values input in to vectors is consistent with the space (e.g. bdry conditions) """
        return values

    # Also need to consider arrays that are whatever size and who cares about the length.
    #... or do we take care of that in the l2 space and extend the notion of ==
    #... or do we create a new routine that is like == but not quite... (yes prob this)
    #... And how about whether this has anything to do with check_values_shape??
    # ALSO WE DON'T INCLUDE INTERPOLATION checking IN GENERAL DOT PRODS
    def is_dot_compatible(self, other):
        # Tests if another LinOp is compatible for dot pro
        return other == self 
      
    def __eq__(self, other):
        # If two spaces are of the same type and have the same dimension, or one
        # has 'free' dimension specified by self.n = None, then they are "equal"
        if isinstance(other, type(self)):
            return self.n == other.n

    def values_dot(self, l_values, r_values, axis=None):
        """ axis has to be of form (i,j) where the dot prod will happen along axes
          i or l_values and axis j of r_values """
        if axis is None:
            if self.G is not None:
                return l_values @ self.G @ r_values
            else:
                return l_values @ r_values
        else: 
            # Fancy multi-index dot prod....
            raise ValueError('sorry multi-index dot not implemented yet!')

    def can_interp(self, other):
        return other == self

    def interp(self, other):
        pass

    def riesz_rep_coeffs(self, lin_comb, normalise=False):
        # Return a vector or a basis of representers given a linear combination
        # or a series of linear combinations by solving the Galerkin 
        # projection using the Gram matrix

        # TODO: Should depend more on shape of data rather than the claims of the 
        # "space" ??
        if lin_comb.ndim == 1 and lin_comb.shape[0] == self.n:
             
            if self.G is None:
                v = lin_comb
            elif scipy.sparse.issparse(self.G):
                v = scipy.sparse.linalg.spsolve(self.G, lin_comb)
            else:
                v = scipy.linalg.solve(self.G, lin_comb, sym_pos=True)
            
            return v

        elif lin_comb.ndim == 2 and lin_comb.shape[0] == self.n:

            if self.G is None:
                vs = lin_comb
            else:
                vs = np.zeros(lin_comb.shape)
                for i, lc in enumerate(lin_comb.T):
                    if scipy.sparse.issparse(self.G):
                        vs[:,i] = scipy.sparse.linalg.spsolve(self.G, lc)
                    else:
                        vs[:,i] = scipy.linalg.solve(self.G, lc, sym_pos=True)

            return vs

        else:
            raise ValueError(f'{self.__class__.__name__}: Invalid linear combination size to find representer')

    def __str__(self):
        if self.n:
            return f'{type(self).__name__}({self.n})'
        else:
            return f'{type(self).__name__}'
    
    def __repr__(self):
        if self.n:
            return f'{type(self).__name__}({self.n})'
        else:
            return f'{type(self).__name__}'

# A solution: l2 is a static global instance of Space with no set size,
#             NoSpace, used for Vector, is a generic space with length 1 
l2 = Space()

class L2DyadicSq(Space):
    
    def __init__(self, div, domain=((0,1),(0,1))):
        
        super().__init__(n = self._side_len(div)**2)
        self.div = div
        self.domain = domain

    @property
    def G(self):
        if self._G is None:
            h = 2 ** (-2 * self.div)
            self._G = h * scipy.sparse.eye(self.n).tocsr()

        return self._G
 
    def can_interp(self, other):
        if hasattr(other, 'space'):
            other = other.space
        
        if isinstance(other, type(self)):
            if other.div <= self.div:
                return True
        else:
            return False

    def interp(self, other):
        """ Simple interpolation routine to make this function on a finer division dyadic grid """
        if other.space.div > self.div:
            raise Exception(f'{self.__class__.__name__}: Interpolation division smaller than input division! \
                             Integration not yet available')
        elif other.space.div == self.div:
            return other
        else:
            div_diff = self.div - other.space.div
            
            if other.values.ndim == 1:
                vals = other.values.reshape((other.space.side_len, other.space.side_len))
                new_vals = vals.repeat(2**div_diff, axis=0).repeat(2**div_diff, axis=1)
                return type(other)(new_vals.flatten(), space=self) 

            elif other.values.ndim == 2:
                interped_vals = np.zeros((self.n, other.n))
                for i, v in enumerate(other):
                    interp_v = other.values.reshape((other.space.side_len, other.space.side_len))
                    interp_v = interp_v.repeat(2**div_diff, axis=0).repeat(2**div_diff, axis=1)
                    interped_vals[:,i] = interp_v(self.x_grid, self.y_grid)[1:-1, 1:-1].flatten()
                return type(other)(interped_vals, space=self)
            else:
                raise ValueError(f'{self.__class__.__name__}: can only interpolate objects of type Vector or Basis')

    @property
    def side_len(self):
        return self._side_len(self.div)
    def _side_len(self, d):
        return 2**d

    @property
    def x_grid(self):
        return self._x_grid(self.side_len)
    def _x_grid(self, sl):
        return np.linspace(self.domain[0][0], self.domain[0][1], sl, endpoint=False) + 0.5 / sl
    
    @property
    def y_grid(self):
        return self._y_grid(self.side_len)
    def _y_grid(self, sl):
        return np.linspace(self.domain[1][0], self.domain[1][1], sl, endpoint=False) + 0.5 / sl

    def plot(self, vector, ax, title=None, div_frame=None, alpha=0.6, cmap=None, clim=None, show_axis_labels=True):
        if cmap is None:
            cmap = cm.get_cmap('viridis')

        # We do some tricks here (i.e. using np.repeat) to plot the piecewise constant nature of the random field...
        x = np.linspace(0.0, 1.0, 2**self.div + 1, endpoint = True).repeat(2)[1:-1]
        xs, ys = np.meshgrid(x, x)
        vals_sq = vector.values.reshape((self.side_len, self.side_len))
        wframe = ax.plot_surface(xs, ys, vals_sq.repeat(2, axis=0).repeat(2, axis=1), cstride=1, rstride=1,
                                 cmap=cmap, alpha=alpha)
        if clim is not None:
            wframe.set_clim(clim[0], clim[1])
        else:
            wframe.set_clim(vector.values.min(), vector.values.max())
        ax.set_facecolor('white')
        if show_axis_labels:
            ax.set_xlabel('$x$')
            ax.set_ylabel('$y$')
        if title is not None:
            ax.set_title(title)


    def __eq__(self, other):
        if isinstance(other, type(self)):
            if other.div == self.div and other.domain == self.domain:
                return True
            else:
                return False
        else:
            return False
    

    def __str__(self):
        return '{0}({1}, {2})'.format(self.__class__.__name__, self.div, self.domain)

class H1DyadicSq(L2DyadicSq):
     
    @property
    def G(self):
        if self._G is None:
            diag = 4.0 * np.ones(self.side_len * self.side_len)
            lr_diag = - np.ones(self.side_len * self.side_len)

            # min_diag is below the diagonal, hence deals with element to the left in the FEM grid
            lr_diag[self.side_len-1::self.side_len] = 0 # These corresponds to edges on left or right extreme
            lr_diag = lr_diag[:-1]

            ud_diag = - np.ones(self.side_len*(self.side_len-1))
            #ud_diag = ud_diag[self.side_len:]
            
            self._G = scipy.sparse.diags([diag, lr_diag, lr_diag, ud_diag, ud_diag], 
                    [0, -1, 1, -self.side_len, self.side_len], format='csr')

        return self._G

    def check_values_shape(self, values, axis):
        """ ensures values are of right size for space if relevant """
        return super().check_values_shape(values, axis) or values.shape[axis] == (self.side_len+2)**2

    def values_format(self, vals, axis):
        """ ensures values input in to vectors is consistent with the space (e.g. boundary conditions) """
        if vals.ndim == 1 and vals.shape[0] == (self.side_len+2)**2:
            vals = vals[self.side_len+2:-(self.side_len+2)]
            vals = np.delete(vals, np.s_[::self.side_len+2])
            vals = np.delete(vals, np.s_[self.side_len::self.side_len+1])
            # Need to do the above but remove completely
        elif vals.ndim > 1 and vals.shape[axis] == (self.side_len+2)**2:
            # This sets the boundaries to zero even if we have a multidimensional
            # linear operator of some form. Uses fancy multi-dim slicing
            vals = np.delets(vals, multisel(slice(None,self.side_len+2), vals.ndim, axis))
            vals = np.delete(vals, multisel(slice(-(self.side_len+2),None), vals.ndim, axis))
            vals = np.delete(vals, multisel(slice(None,None,self.side_len+2), vals.ndim, axis))
            vals = np.delete(vals, multisel(slice(self.side_len,None,self.side_len+1), vals.ndim, axis))
        return vals
   
    def interp(self, other):
        """ Simple interpolation routine to make this function on a finer division dyadic grid """

        # TODO: make this interp scheme work on any arbitrary values array with 
        # an axis specified, thus being able to interpolate Basis and LinOp's 
        if other.space.div > self.div:
            raise Exception('{0}: Interpolation division smaller than field division! Integration not yet available'\
                            .format(self.__class__.__name__))
        elif other.space.div == self.div:
            return other
        else:
            if other.values.ndim == 1:
                interp_vals = other.values.reshape((other.space.side_len, other.space.side_len))
                interp_vals = np.pad(interp_vals, ((1,1),(1,1)), 'constant')
                interp_func = scipy.interpolate.RectBivariateSpline(other.space.x_grid, other.space.y_grid, interp_vals, kx=1, ky=1)
                return type(other)(interp_func(self.x_grid, self.y_grid)[1:-1, 1:-1].flatten(), space=self)

            elif other.values.ndim == 2:
                interped_vals = np.zeros((self.n, other.n))
                for i, v in enumerate(other):
                    interp_v = v.values.reshape((other.space.side_len, other.space.side_len))
                    interp_v = np.pad(interp_v, ((1,1),(1,1)), 'constant')
                    interp_func = scipy.interpolate.RectBivariateSpline(other.space.x_grid, other.space.y_grid, interp_v, kx=1, ky=1)
                    interped_vals[:,i] = interp_func(self.x_grid, self.y_grid)[1:-1, 1:-1].flatten()
                return type(other)(interped_vals, space=self)
            else:
                raise ValueError(f'{self.__class__.__name__}: can only interpolate objects of type Vector or Basis')
    
    def _side_len(self, d):
        return 2**d-1

    def _x_grid(self, sl):
        return np.linspace(self.domain[0][0], self.domain[0][1], sl+2, endpoint=True)

    def _y_grid(self, sl):
        return np.linspace(self.domain[1][0], self.domain[1][1], sl+2, endpoint=True)

    def plot(self, vector, ax, title=None, div_frame=4, alpha=0.6, cmap=None, clim=None, show_axis_labels=True):
        if cmap is None:
            cmap = cm.get_cmap('viridis')

        xs, ys = np.meshgrid(self.x_grid, self.y_grid)

        plot_values = np.pad(vector.values.reshape((self.side_len, self.side_len)), ((1,1),(1,1)), 'constant')
        if self.div > div_frame:
            wframe = ax.plot_surface(xs, ys, plot_values, cstride=2**(self.div - div_frame),\
                                     rstride=2**(self.div-div_frame), cmap=cmap, alpha=alpha)
        else:
            wframe = ax.plot_surface(xs, ys, plot_values, cstride=1, rstride=1, cmap=cmap, alpha=alpha)
        
        if clim is not None:
            wframe.set_clim(clim[0], clim[1])
        else:
            wframe.set_clim(vector.values.min(), vector.values.max())
        ax.set_facecolor('white')
        if show_axis_labels:
            ax.set_xlabel('$x$')
            ax.set_ylabel('$y$')
        if title is not None:
            ax.set_title(title)


class Element(object):
    """ For vectors that are made up of "exact" functions, we allow them to be sums of
        Elements, typically some simple function like sin, delta or polynomial. This
        covers a surprising amount of functional analysis and algorithms. The main thing
        is that we have a hashing function by parameter type so that we can use the dictionary
        approach in ExactVector """

    def __init__(self):

        self.d = None 
        self.domain = None
        self.space = None

    def dot(self, right, left_params, right_params):
        """ These exact functions have mathematically defined dot products"""
        pass

    def evaluate(self, params, x):
        """ This returns an array of len(params) * len(x), i.e. x is the 2nd coord,
            which is an important distinction between Element and Vector:
            Vector will return an array of len(x), summing over all elements.
            Here we use evaluate for dot products, so must provide all evaluation """
        pass

    def _normaliser(self, params):
        pass

    def _delta_dot(self, left, left_params, right_params):
        """ Dotting with a delta function is always the same... """
        rc = right_params.values_array()

        x0 = left_params.keys_array()
        lc = left_params.values_array()[:,np.newaxis]
        ln = left._normaliser(left_params)[:,np.newaxis]
        
        return (ln * lc * self.evaluate(right_params, x0)).sum()

    def _avg_dot(self, right, left_params, right_params):
        # This is another one that the function should know about - how to do a
        # local average / integration
        pass

    def __hash__(self):
        return hash((type(self), self.d, self.domain))

    def __eq__(self, other):
        return type(self) is type(other) and self.d == other.d and self.domain == other.domain

    def __ne__(self, other):
        return not(self == other)

    def __str__(self):
        return self.__class__.__name__


